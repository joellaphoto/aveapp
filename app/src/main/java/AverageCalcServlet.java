import java.io.IOException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

import javax.inject.Inject;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/app")
public class AverageCalcServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private float average;

  @Inject
  private AverageCalcService averageCalcService;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
		response.setContentType("text/html");

		response.getWriter().println("<!DOCTYPE html>");
		response.getWriter().println("<html>");
		response.getWriter().println("  <head>");
		response.getWriter().println("    <meta charset=\"utf-8\">");
		response.getWriter().println("    <title>Räkna ut medelvärdet på 3 tal</title>");
		response.getWriter().println("  </head>");
		response.getWriter().println("  <body>");
		response.getWriter().println("    <form method=\"post\" accept-charset=\"utf-8\">");
		response.getWriter().println("    	<h4>Skriv in tre tal och få reda på medelvärdet</h4>");
		response.getWriter().println("    	<input type=\"text\" name=\"tal1\" placeholder=\"första talet\"></input>");
		response.getWriter().println("    	<input type=\"text\" name=\"tal2\" placeholder=\"andra talet\"></input>");
		response.getWriter().println("    	<input type=\"text\" name=\"tal3\" placeholder=\"tredje talet\"></input>");
		response.getWriter().println("    	<input type=\"submit\" value=\"submit\"></input>");
		response.getWriter().println("    </form>");
		response.getWriter().println("  </body>");
		response.getWriter().println("</html>");
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
		float tal1 = Float.parseFloat(request.getParameter("tal1"));
		float tal2 = Float.parseFloat(request.getParameter("tal2"));
		float tal3 = Float.parseFloat(request.getParameter("tal3"));
		average = averageCalcService.averageCalc(tal1, tal2, tal3);

		response.setContentType("text/html");

		response.getWriter().println("<!DOCTYPE html>");
		response.getWriter().println("<html>");
		response.getWriter().println("  <head>");
		response.getWriter().println("    <meta charset=\"utf-8\">");
		response.getWriter().println("    <title>Resultat</title>");
		response.getWriter().println("  </head>");
		response.getWriter().println("  <body>");
		response.getWriter().println("    <h4>medelvärdet: " + average +"</h4>");
		response.getWriter().println("  </body>");
		response.getWriter().println("</html>");

	}
}
