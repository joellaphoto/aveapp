import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Stateless
public class AverageCalcService{
  private float average;

  @PersistenceContext(unitName = "app")
  private EntityManager em;

  public float averageCalc(float num1, float num2, float num3){
    AverageCalc averageNum1 = new AverageCalc();
    AverageCalc averageNum2 = new AverageCalc();
    AverageCalc averageNum3 = new AverageCalc();
    averageNum1.setNum(num1);
    averageNum2.setNum(num2);
    averageNum3.setNum(num3);

    float sum = averageNum1.getNum() + averageNum2.getNum() + averageNum3.getNum();
    average = sum/3;

    AverageCalc theAverage = new AverageCalc();
    theAverage.setNum(average);
    em.persist(theAverage);
    return average;

  }
}
