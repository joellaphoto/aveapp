import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Entity
public class AverageCalc implements Serializable{
  @GeneratedValue
  @Id
  private int id;

  @NotNull
  private float num;

  public int getId(){
    return id;
  }

  public float getNum(){
    return num;
  }
  public void setNum(float num){
    this.num = num;
  }


}
